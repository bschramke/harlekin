\contentsline {chapter}{\numberline {1}Harlekin Game Engine}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Content}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Foreword}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}3rd Party Lib and Licensing Information}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Motivation}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Architecture Overview}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Getting Started}{2}{section.1.6}
\contentsline {section}{\numberline {1.7}Foundation Layer Subsystems}{2}{section.1.7}
\contentsline {section}{\numberline {1.8}Render Layer Subsystems}{2}{section.1.8}
\contentsline {section}{\numberline {1.9}Application Layer Subsystems}{2}{section.1.9}
\contentsline {section}{\numberline {1.10}How To Compile}{2}{section.1.10}
\contentsline {chapter}{\numberline {2}Namespace Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Namespace List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Hierarchical Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class Hierarchy}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Data Structure Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Data Structures}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}File Index}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}File List}{9}{section.5.1}
\contentsline {chapter}{\numberline {6}Namespace Documentation}{11}{chapter.6}
\contentsline {section}{\numberline {6.1}Harlekin Namespace Reference}{11}{section.6.1}
\contentsline {section}{\numberline {6.2}Harlekin\discretionary {-}{}{}:\discretionary {-}{}{}:Core Namespace Reference}{11}{section.6.2}
\contentsline {chapter}{\numberline {7}Data Structure Documentation}{13}{chapter.7}
\contentsline {section}{\numberline {7.1}Harlekin\discretionary {-}{}{}:\discretionary {-}{}{}:Core\discretionary {-}{}{}:\discretionary {-}{}{}:Application Class Reference}{13}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Member Function Documentation}{13}{subsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.1.1}run}{13}{subsubsection.7.1.1.1}
\contentsline {section}{\numberline {7.2}Harlekin\discretionary {-}{}{}:\discretionary {-}{}{}:Core\discretionary {-}{}{}:\discretionary {-}{}{}:Game\discretionary {-}{}{}App Class Reference}{13}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Constructor \& Destructor Documentation}{14}{subsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.1.1}Game\discretionary {-}{}{}App}{14}{subsubsection.7.2.1.1}
\contentsline {subsubsection}{\numberline {7.2.1.2}$\sim $\discretionary {-}{}{}Game\discretionary {-}{}{}App}{14}{subsubsection.7.2.1.2}
\contentsline {chapter}{\numberline {8}File Documentation}{15}{chapter.8}
\contentsline {section}{\numberline {8.1}/mnt/workspace/\discretionary {-}{}{}C++/\discretionary {-}{}{}Engines/harlekin/code/core/\discretionary {-}{}{}Game\discretionary {-}{}{}App.cpp File Reference}{15}{section.8.1}
\contentsline {section}{\numberline {8.2}/mnt/workspace/\discretionary {-}{}{}C++/\discretionary {-}{}{}Engines/harlekin/code/include/harlekin/core/\discretionary {-}{}{}Application.hpp File Reference}{15}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Macro Definition Documentation}{15}{subsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.1.1}H\discretionary {-}{}{}A\discretionary {-}{}{}R\discretionary {-}{}{}L\discretionary {-}{}{}E\discretionary {-}{}{}K\discretionary {-}{}{}I\discretionary {-}{}{}N\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}C\discretionary {-}{}{}O\discretionary {-}{}{}R\discretionary {-}{}{}E\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}P\discretionary {-}{}{}P\discretionary {-}{}{}L\discretionary {-}{}{}I\discretionary {-}{}{}C\discretionary {-}{}{}A\discretionary {-}{}{}T\discretionary {-}{}{}I\discretionary {-}{}{}O\discretionary {-}{}{}N\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}H\discretionary {-}{}{}P\discretionary {-}{}{}P}{15}{subsubsection.8.2.1.1}
\contentsline {section}{\numberline {8.3}/mnt/workspace/\discretionary {-}{}{}C++/\discretionary {-}{}{}Engines/harlekin/code/include/harlekin/core/\discretionary {-}{}{}Game\discretionary {-}{}{}App.hpp File Reference}{15}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Macro Definition Documentation}{16}{subsection.8.3.1}
\contentsline {subsubsection}{\numberline {8.3.1.1}H\discretionary {-}{}{}A\discretionary {-}{}{}R\discretionary {-}{}{}L\discretionary {-}{}{}E\discretionary {-}{}{}K\discretionary {-}{}{}I\discretionary {-}{}{}N\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}C\discretionary {-}{}{}O\discretionary {-}{}{}R\discretionary {-}{}{}E\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}G\discretionary {-}{}{}A\discretionary {-}{}{}M\discretionary {-}{}{}E\discretionary {-}{}{}A\discretionary {-}{}{}P\discretionary {-}{}{}P\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}H\discretionary {-}{}{}P\discretionary {-}{}{}P}{16}{subsubsection.8.3.1.1}
\contentsline {section}{\numberline {8.4}/mnt/workspace/\discretionary {-}{}{}C++/\discretionary {-}{}{}Engines/harlekin/doc/harlekin/mainpage.dox File Reference}{16}{section.8.4}
\contentsline {part}{Index}{17}{section*.13}
