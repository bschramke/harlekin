<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>GameApp.cpp</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/core/</path>
    <filename>_game_app_8cpp</filename>
    <includes id="_game_app_8hpp" name="GameApp.hpp" local="yes" imported="no">harlekin/core/GameApp.hpp</includes>
    <namespace>Harlekin</namespace>
    <namespace>Harlekin::Core</namespace>
  </compound>
  <compound kind="file">
    <name>Application.hpp</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/core/</path>
    <filename>_application_8hpp</filename>
    <class kind="class">Harlekin::Core::Application</class>
    <namespace>Harlekin</namespace>
    <namespace>Harlekin::Core</namespace>
    <member kind="define">
      <type>#define</type>
      <name>HARLEKIN_CORE_APPLICATION_HPP</name>
      <anchorfile>_application_8hpp.html</anchorfile>
      <anchor>a3e527c919ebc10a16d5b2a788c1bf00b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>GameApp.hpp</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/core/</path>
    <filename>_game_app_8hpp</filename>
    <includes id="_application_8hpp" name="Application.hpp" local="yes" imported="no">harlekin/core/Application.hpp</includes>
    <class kind="class">Harlekin::Core::GameApp</class>
    <namespace>Harlekin</namespace>
    <namespace>Harlekin::Core</namespace>
    <member kind="define">
      <type>#define</type>
      <name>HARLEKIN_CORE_GAMEAPP_HPP</name>
      <anchorfile>_game_app_8hpp.html</anchorfile>
      <anchor>a1d557ad078513a1943d4e394fe013be7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>mainpage.dox</name>
    <path>/mnt/workspace/C++/Engines/harlekin/doc/harlekin/</path>
    <filename>mainpage_8dox</filename>
  </compound>
  <compound kind="namespace">
    <name>Harlekin</name>
    <filename>namespace_harlekin.html</filename>
    <namespace>Harlekin::Core</namespace>
  </compound>
  <compound kind="namespace">
    <name>Harlekin::Core</name>
    <filename>namespace_harlekin_1_1_core.html</filename>
    <class kind="class">Harlekin::Core::Application</class>
    <class kind="class">Harlekin::Core::GameApp</class>
  </compound>
  <compound kind="class">
    <name>Harlekin::Core::Application</name>
    <filename>class_harlekin_1_1_core_1_1_application.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>class_harlekin_1_1_core_1_1_application.html</anchorfile>
      <anchor>ae073cc68c1fe75b1fb6623c1f9692b32</anchor>
      <arglist>()=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Harlekin::Core::GameApp</name>
    <filename>class_harlekin_1_1_core_1_1_game_app.html</filename>
    <base>Harlekin::Core::Application</base>
    <member kind="function">
      <type></type>
      <name>GameApp</name>
      <anchorfile>class_harlekin_1_1_core_1_1_game_app.html</anchorfile>
      <anchor>a874c0fa472a7c5c66dc6ff7a72ff75ea</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~GameApp</name>
      <anchorfile>class_harlekin_1_1_core_1_1_game_app.html</anchorfile>
      <anchor>a976fd752d6fc52564c9fb6ae4c5a6bb5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>/mnt/workspace/C++/Engines/harlekin/code</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/</path>
    <filename>dir_050edd66366d13764f98250ef6db77f6.html</filename>
    <dir>/mnt/workspace/C++/Engines/harlekin/code/core</dir>
    <dir>/mnt/workspace/C++/Engines/harlekin/code/include</dir>
  </compound>
  <compound kind="dir">
    <name>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/core</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/core/</path>
    <filename>dir_85e9c10631cd33e1b87c4b84bde5dec7.html</filename>
    <file>Application.hpp</file>
    <file>GameApp.hpp</file>
  </compound>
  <compound kind="dir">
    <name>/mnt/workspace/C++/Engines/harlekin/code/core</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/core/</path>
    <filename>dir_e696cd3ad997450e9142ed5a043995b9.html</filename>
    <file>GameApp.cpp</file>
  </compound>
  <compound kind="dir">
    <name>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/</path>
    <filename>dir_b3971bca3e331d5f4949fead094e7149.html</filename>
    <dir>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin/core</dir>
  </compound>
  <compound kind="dir">
    <name>/mnt/workspace/C++/Engines/harlekin/code/include</name>
    <path>/mnt/workspace/C++/Engines/harlekin/code/include/</path>
    <filename>dir_f8a5fb882cc5ea0f84873b4bfbe1e42c.html</filename>
    <dir>/mnt/workspace/C++/Engines/harlekin/code/include/harlekin</dir>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Harlekin Game Engine</title>
    <filename>index</filename>
    <docanchor file="index" title="Content">HarlekinContent</docanchor>
    <docanchor file="index" title="Foreword">HarlekinForeword</docanchor>
    <docanchor file="index" title="3rd Party Lib and Licensing Information">HarlekinExtLibs</docanchor>
    <docanchor file="index" title="Motivation">HarlekinMotivation</docanchor>
    <docanchor file="index" title="Architecture Overview">HarlekinArchitecture</docanchor>
    <docanchor file="index" title="Getting Started">GettingStarted</docanchor>
    <docanchor file="index" title="Foundation Layer Subsystems">FoundationLayer</docanchor>
    <docanchor file="index" title="Render Layer Subsystems">RenderLayer</docanchor>
    <docanchor file="index" title="Application Layer Subsystems">ApplicationLayer</docanchor>
    <docanchor file="index" title="How To Compile">HarlekinCompiling</docanchor>
  </compound>
</tagfile>
