#-------------------------------------------------
#
# Project created by QtCreator 2013-05-02T16:15:47
#
#-------------------------------------------------

QT       -= core gui

TARGET = HarlekinCore
TEMPLATE = lib

INCLUDEPATH += $$PWD/include

DEFINES += HARLEKIN_LIBRARY

QMAKE_CXXFLAGS += -std=c++11


SOURCES += \   
    src/Application.cpp \
    src/Configuration.cpp \
    src/Arguments.cpp

HEADERS += \ 
    include/Harlekin/Core/Singleton.hpp \
    include/Harlekin/Core/Application.hpp \
    include/Harlekin/Core/Configuration.hpp \
    include/Harlekin/Core/Arguments.hpp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
