# Harlekin Game Engine

coming soon

License
=======
Harlekin is made available under the zlib License.

Copyright (c) 2013-2014 Björn Schramke

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

<ol>
  <li>The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.</li>
  <li>Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.</li>
  <li>This notice may not be removed or altered from any source distribution.</li>
</ol>