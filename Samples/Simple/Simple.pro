#-------------------------------------------------
#
# Project created by QtCreator 2013-05-02T16:15:47
#
#-------------------------------------------------
TARGET = SimpleDemo
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += $$PWD/include

DEFINES += HARLEKIN_SAMPLEAPP

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    src/main.cpp

#-------------------------------------------------
# Include library HarlekinCore
#-------------------------------------------------
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../HarlekinCore/release/ -lHarlekinCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../HarlekinCore/debug/ -lHarlekinCore
else:unix: LIBS += -L$$OUT_PWD/../../HarlekinCore/ -lHarlekinCore

INCLUDEPATH += $$PWD/../../HarlekinCore/include
DEPENDPATH += $$PWD/../../HarlekinCore
